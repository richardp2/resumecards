---
type: "Education"
heading: "University of Wales, Cardiff"
subheading: "PgC Sustainable Leadership, Organizational Leadership"
duration: "November 2012 – November 2013 (1 year)"
location: "Cardiff, United Kingdom"
---

A Postgraduate course in Leadership designed specifically for professionals working in the
Construction Industry. The main outcome of the course was the production of a Strategic Growth
Project specifically looking at the growth potential of the company over the course of 3 years. This
was a very interesting course that has helped me immensely in developing a more strategic outlook 
with regards to the future of the business, as well as a much better understanding of the concepts
of leadership and management.

Linked to the [20Twenty Leadership for Construction Course](http://www.leadershipmanagementtraining.org.uk/)