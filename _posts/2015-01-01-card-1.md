---
type: "Work Experience"
heading: "C.P. Construction (Gwent) Ltd"
subheading: "Quantity Surveyor"
duration: "November 2011 – Present"
location: "Newport, United Kingdom"
---

In November 2011 I took the opportunity to move into the family business. It is a small construction
company, so there is a lot more variety of work and the projects are much smaller. I am responsible
for pricing of enquiries, managing projects & project finances and looking for new work.

I have also had the opportunity to work on a database for keeping track of quotes, orders,
valuations etc for jobs from enquiry stage through to final account. I have increased the usability
of the system and trained the rest of the management team in how to use it.
