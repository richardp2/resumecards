## ResumeCards

ResumeCards is a Markdown based resume generator. It looks great on mobile/desktop and can be saved
as PDF. I've forked the main project from [Elle Kasai][1] because it looked brilliant and I thought
I should have an alternative to LinkedIn should things go wrong for me in the future. I hope that I
don't need a resume/CV ever again, but it's always a good idea to have a backup.

### Original Author & License

Elle Kasai

- [Website](http://ellekasai.com/about)
- [Twitter](http://twitter.com/ellekasai)

[MIT License](http://ellekasai.mit-license.org).

[1]: https://github.com/ellekasai/resumecards